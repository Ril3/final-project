DROP TABLE IF EXISTS facture;
CREATE TABLE facture (  
    id int NOT NULL primary key AUTO_INCREMENT comment 'primary key',
    name varchar(255) NOT NULL,
    description varchar(255) NOT NULL,
    price DECIMAL
) default charset utf8 comment '';

INSERT INTO facture (name, description, price)
VALUES ("TestName", "description", 232.23);


DROP TABLE IF EXISTS manual_product;

CREATE TABLE manual_product (
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT 'primary key',
    name VARCHAR(255) NOT NULL,
    price DECIMAL,
    photo VARCHAR(255),
    factureID INT,
    FOREIGN KEY (factureID) REFERENCES facture(id)
) default charset utf8 comment '';