
export class ManualProduct {
    id:Number;
    name: String;
    price: Number;
    photo: String;
    factureID: Number;

    constructor(name:String, price:Number, photo: String, id:Number=null, factureID:Number=null){
        this.name = name;
        this.price = price;
        this.photo = photo;
        this.id = id;
        this.factureID = factureID

    }
}