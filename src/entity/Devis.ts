import Joi from "joi";
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Product } from "./Product";

@Entity()
export class Devis {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column({type: 'float'})
    price: number;

    @Column()
    description: string;

    @Column()
    email: string;

    @OneToMany(()=> Product, product => product.devis)
    products: Product[];
}

export const devisSchema = Joi.object({
    name: Joi.string()
    .min(3)
    .max(200)
    .required(),

    price: Joi.number()
    .greater(-1)
    .required(),

    description: Joi.string()
    .min(3)
    .max(200)
    .required(),

    email: Joi.string()
    .min(3)
    .max(200)
    .required()
})