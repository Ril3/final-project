import Joi from "joi";
import { Column, Entity, JoinTable, ManyToMany, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Element } from "./Element";
import { Product } from "./Product";

@Entity()
export class Commande {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({type:'float'})
    total: number; 

    @Column()
    description: string;

    @Column()
    email: string;

    
    @OneToMany(()=> Product, product => product.commande)
    products: Product[];

    @ManyToMany(()=> Element, element => element.commande)
    @JoinTable()
    elements: Element[];
}

export const commandeSchema = Joi.object({

    total: Joi.number()
    .greater(-1)
    .required(),

    description: Joi.string()
    .min(3)
    .max(200)
    .required(),
    
    elements: Joi.array(),

    email: Joi.string().email().required()

})