import Joi from "joi";
import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Category } from "./Category";
import { Commande } from "./Commande";
import { Devis } from "./Devis";
import { Element } from "./Element";


@Entity()
export class Product {

    @PrimaryGeneratedColumn({ type: "int" })
    id:number;

    @Column()
    name: string;

    @Column({type: 'float'} )
    price: number;

    @Column()
    photo: string;

    @Column()
    customizable: boolean;


    @ManyToOne(() => Category, category => category.products)
    categories: Category;

    @OneToMany(() => Element, element => element.products)
    elements: Element[]; 

    @ManyToOne(()=> Commande, commande => commande.products)
    commande: Commande;

    @ManyToOne(()=> Devis, devis => devis.products)
    devis: Devis;
}


export const productSchema = Joi.object({

    name: Joi.string()
    .min(3)
    .max(200)
    .required(),

    photo: Joi.string()
    .min(3)
    .max(200)
    .required(),

    price: Joi.number()
    .greater(-1)
    .required(),

    categories: Joi.number()
        .greater(0)

})