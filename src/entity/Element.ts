import Joi from "joi";
import { Column, Entity, JoinTable, ManyToMany, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Commande } from "./Commande";
import { Product } from "./Product";


@Entity()
export class Element {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column({type: 'float'})
    price: number;

    @Column()
    description: string;

    @ManyToOne(() => Product, product => product.elements)
    products: Product;

    @ManyToMany(()=> Commande, commande => commande.elements)
    commande: Commande[];
}

export const elementSchema = Joi.object({
    name: Joi.string()
    .min(3)
    .max(200)
    .required(),

    price: Joi.number()
    .greater(-1)
    .required(),

    description: Joi.string()
    .min(3)
    .max(200)
    .required(),

    commande: Joi.array()
})