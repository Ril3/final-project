export class Facture {
    id:Number;
    name: String;
    description:String;
    price: Number;

    constructor(name:String, description:String, price: Number, id:Number=null){
        this.name = name;
        this.description = description;
        this.price = price;
        this.id = id;
    }
}