import Joi from "joi";
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Product } from "./Product";


@Entity()
export class Category {

    @PrimaryGeneratedColumn({ type: "int" })
    id: number;

    @Column()
    label: string;

    @Column()
    image: string;

    @Column()
    description: string;

    @OneToMany(() => Product, product => product.categories)
    products: Product[];

}


export const categorySchema = Joi.object({
    label: Joi.string()
    .min(3)
    .max(200)
    .required(),

    image: Joi.string()
    .min(3)
    .max(400)
    .required(),

    description: Joi.string()
    .min(3)
    .max(400)
    .required()
})