import { Facture } from "../entity/Facture";
import { connection } from "./connection";


export default class FactureRepository{
    //Find all factures
    static async findAll(){
        const [rows]:any  = await connection.query('SELECT * FROM facture');
        return rows.map(row => new Facture(row.name, row.description, row.price, row.id));
    }

    //Find one facture by id
    static async findOne(id:any){
            const [rows]:any = await connection.execute('SELECT * FROM facture WHERE id=?', [id]);
            if(rows.length === 1){
                return new Facture(rows[0].name, rows[0].description, rows[0].price, rows[0].id)
            }
            return null;
    }

    //ADD facture
    static async add(fact:any){
            console.log(fact)
                await connection.execute('INSERT INTO facture (name, description, price) VALUES (?,?,?)', 
                [fact.name, fact.description, fact.price]);  
    }
 
    //UPDATE facture
    static async update(factu:any){
        const [rows] = await connection.execute('UPDATE facture SET name=?, description=?, price=? WHERE id=?', 
        [factu.name, factu.description, factu.price, factu.id])
    }

    //DELETE facture
    static async delete(id:any){
        await connection.execute('DELETE FROM facture WHERE id=?', [id]);
    }
}