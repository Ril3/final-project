import { ManualProduct } from "../entity/ManualProduct";
import { connection } from "./connection";


export default class ManualProductRepo {

    //Find ALL
    static async findAll (){
        const [rows]:any = await connection.query("SELECT * FROM manual_product");
        const listDeMan = [];
        for(let row of rows){
          listDeMan.push( new ManualProduct(row.name, row.price, row.photo, row.id, row.factureID))
        }
        return listDeMan;
    }

    //ADD Product 
    static async addProduct(prod:any){
        await connection.query('INSERT INTO manual_product (name, price, photo, factureID) VALUES (?,?,?,?)', 
        [prod.name, prod.price, prod.photo, prod.factureID])
    }
}

    