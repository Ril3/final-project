import { Router } from "express";
import { getRepository } from "typeorm";
import { Devis, devisSchema } from "../entity/Devis";



export const devisController = Router();

//Find all devis
devisController.get('/', async (req,res)=>{
    try {
        const devis = await getRepository(Devis).find({ relations: ['products'] })
        res.json(devis)
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})
//Find one devis
devisController.get('/:id', async (req, res) => {
    try {
        const devis = await getRepository(Devis).findOne(req.params.id, { relations: ['products'] });
        if (!devis) {
            res.status(404).end();
            return;
        }
        res.json(devis);
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
});

//ADD devis
devisController.post('/', async (req, res) => {
    console.log(req.body)
    try {
        // Validation
        const { error } = devisSchema.validate(req.body, { abortEarly: false });

        if (error) {
            res.status(400).json({ error: error.details.map(item => item.message) });
            return;
        }

        let devis = new Devis();
        Object.assign(devis, req.body)
        await getRepository(Devis).save(devis);
        res.status(201).json(devis);

    } catch (error) {

        console.log(error);
        res.status(500).json(error);
    }
})

//UPDATE devis
devisController.patch("/:id", async(req, res)=>{
    try {
        const repo = getRepository(Devis)
        const devis = await repo.findOne(req.params.id)
        if(!devis){
            res.status(404).end();
            return;
        }
        Object.assign(devis, req.body)
        repo.save(devis);
        res.json(devis)
    } catch (error) {
        console.log(error)
        res.status(500).json(error);
    }
})
//DELETE devis
devisController.delete('/:id', async (req, res) => {
    try {
        const repo = getRepository(Devis);
        const result = await repo.delete(req.params.id)
        if (result.affected !== 1) {
            res.status(404).end();
            return;
        }

        res.status(204).end()
    } catch (error) {
        console.log(error)
        res.status(500).json(error);
    }
})
