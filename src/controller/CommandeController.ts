import { Router } from "express";
import { getRepository } from "typeorm";
import { Commande, commandeSchema } from "../entity/Commande";


export const commandeController = Router();

//Find all commandes
commandeController.get('/', async (req,res)=>{
    try {
        const product = await getRepository(Commande).find({ relations: ['products','elements'] })
        res.json(product)
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})

//Find one commandes
commandeController.get('/:id', async (req, res) => {
    try {
        const commande = await getRepository(Commande).findOne(req.params.id, { relations: ['products', 'elements'] });
        if (!commande) {
            res.status(404).end();
            return;
        }
        res.json(commande);
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
});

//ADD commande
commandeController.post('/', async (req, res) => {

    try {
        // Validation
        const { error } = commandeSchema.validate(req.body, { abortEarly: false });

        if (error) {
            res.status(400).json({ error: error.details.map(item => item.message) });
            console.log(req.body)
            return;
        }
        console.log(req.body)
        let commande = new Commande();
        Object.assign(commande, req.body)
        await getRepository(Commande).save(commande);
        res.status(201).json(commande);


    } catch (error) {

        console.log(error);
        res.status(500).json(error);
    }
})

//UPDATE commande
commandeController.patch('/:id', async (req, res) => {
    try {
        const repo = getRepository(Commande);
        const commande = await repo.findOne(req.params.id);
        if (!commande) {
            res.status(404).end();
            return;
        }
        Object.assign(commande, req.body);
        repo.save(commande);
        res.json(commande);
    } catch (error) {
        console.log(error)
        res.status(500).json(error);

    }
})
//DELETE commande
commandeController.delete('/:id', async (req, res) => {
    try {
        const repo = getRepository(Commande);
        const result = await repo.delete(req.params.id)
        if (result.affected !== 1) {
            res.status(404).end();
            return;
        }

        res.status(204).end()
    } catch (error) {
        console.log(error)
        res.status(500).json(error);

    }
})
