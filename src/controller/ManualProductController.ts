import { Router } from "express";
import ManualProductRepo from "../repository/ManualProductRepo";



export const manualProductController = Router();

manualProductController.get('/', async (req, res)=>{
    try {
        const products = await ManualProductRepo.findAll();
        res.json(products)
    } catch (error) {
        console.log(error)
        res.status(500).json(error)
    } 
})


manualProductController.post('/', async (req, res)=>{
    try {
        await ManualProductRepo.addProduct(req.body);
        res.end();
    } catch (error) {
        console.log(error)
        res.status(500).end()
    }
})