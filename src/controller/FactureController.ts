import { Router } from "express";
import FactureRepository from "../repository/FactureRepo";


export const factureController = Router()


//Find all Factures
factureController.get('/', async (req, res) => {
    try{
        const facture = await FactureRepository.findAll();
        res.json(facture)
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
});


//Find one Facture
factureController.get('/:id', async (req, res)=>{
    let facture = await FactureRepository.findOne(req.params.id);
    if(!facture){
        res.status(404).json ({error : 'facture is not found!!!'});
        return;
    }
    res.json(facture);
})  

//Add facture
factureController.post("/", async (req,res) => {
    await FactureRepository.add(req.body);
    res.end()
});

//UPDATE Facture
factureController.patch('/', async (req,res)=>{
    await FactureRepository.update(req.body)
    res.end();
})
 
//DELETE Facture
factureController.delete('/:id', async (req, res)=>{
    try {
        await FactureRepository.delete(req.params.id);
        res.status(204).end();
    } catch (error) {
        console.log(error)
        res.status(500).end()
    }
})