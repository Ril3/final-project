import { Router } from "express";
import { getRepository, Like } from "typeorm";
import { Element, elementSchema } from "../entity/Element";


export const elementController = Router();


//Finda all elements
elementController.get('/', async (req,res)=>{
    try {
        const elements = await getRepository(Element).find({ relations: ['products', 'commande'] })
        res.json(elements)
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})

// Find one element by id
elementController.get('/:id', async (req, res) => {
    try {
        const element = await getRepository(Element).findOne(req.params.id, { relations: ['products', 'commande'] });
        if (!element) {
            res.status(404).end();
            return;
        }
        res.json(element);
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
});

// ADD element
elementController.post('/', async (req, res) => {

    try {
        // Validation
        const { error } = elementSchema.validate(req.body, { abortEarly: false });

        if (error) {
            res.status(400).json({ error: error.details.map(item => item.message) });
            return;
        }

        let element = new Element();
        Object.assign(element, req.body)
        await getRepository(Element).save(element);
        res.status(201).json(element);


    } catch (error) {

        console.log(error);
        res.status(500).json(error);
    }
})

// UPDATE element
elementController.patch('/:id', async (req, res) => {
    try {
        const repo = getRepository(Element);
        const element = await repo.findOne(req.params.id);
        if (!element) {
            res.status(404).end();
            return;
        }
        Object.assign(element, req.body);
        repo.save(element);
        res.json(element);
    } catch (error) {
        console.log(error)
        res.status(500).json(error);

    }
})

// DELETE element
elementController.delete('/:id', async (req, res) => {
    try {
        const repo = getRepository(Element);
        const result = await repo.delete(req.params.id)
        if (result.affected !== 1) {
            res.status(404).end();
            return;
        }

        res.status(204).end()
    } catch (error) {
        console.log(error)
        res.status(500).json(error);

    }
})

//Get elements by a input value that come from front  search bar
elementController.get("/search/:val", async (req, res) => {
    try {
        const elements = await getRepository(Element).find({ where: { name: Like(`%${req.params.val}%`) } })

        res.json(elements)
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})

