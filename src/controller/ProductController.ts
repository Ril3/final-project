import { Router } from "express";
import { getRepository } from "typeorm";
import { Product, productSchema } from "../entity/Product";

export const productController = Router();

//Find all products
productController.get('/', async (req,res)=>{
    try {
        const product = await getRepository(Product).find({ relations: ['categories', 'commande', 'elements', 'devis'] })
        res.json(product)
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})

//Find all products in design category
productController.get('/alldesign', async (req,res)=>{
    try {
        const product = await getRepository(Product).find({ where: { categories: 3 }, relations: ['categories', 'commande', 'elements', 'devis'] })
        res.json(product)
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})

productController.get('/alldevapp', async (req,res)=>{
    try {
        const product = await getRepository(Product).find({ where: { categories: 5 }, relations: ['categories', 'commande', 'elements', 'devis'] })
        res.json(product)
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})

//Find all products in web development category
productController.get('/allwebdev', async (req,res)=>{
    try {
        const product = await getRepository(Product).find({ where: { categories: 4 }, relations: ['categories', 'commande', 'elements', 'devis'] })
        res.json(product)
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})

//Find one product
productController.get('/:id', async (req, res) => {
    try {
        const product = await getRepository(Product).findOne(req.params.id, { relations: ['categories', 'commande', 'elements', 'devis'] });
        if (!product) {
            res.status(404).end();
            return;
        }
        res.json(product);
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
});
//ADD product
productController.post('/', async (req, res) => {

    try {
        // Validation
        const { error } = productSchema.validate(req.body, { abortEarly: false });

        if (error) {
            res.status(400).json({ error: error.details.map(item => item.message) });
            return;
        }

        let product = new Product();
        Object.assign(product, req.body)
        await getRepository(Product).save(product);
        res.status(201).json(product);


    } catch (error) {

        console.log(error);
        res.status(500).json(error);
    }
})
//UPDATE product
productController.patch('/:id', async (req, res) => {
    try {
        const repo = getRepository(Product);
        const product = await repo.findOne(req.params.id);
        if (!product) {
            res.status(404).end();
            return;
        }
        Object.assign(product, req.body);
        repo.save(product);
        res.json(product);
    } catch (error) {
        console.log(error)
        res.status(500).json(error);

    }
})

//DELETE product
productController.delete('/:id', async (req, res) => {
    try {
        const repo = getRepository(Product);
        const result = await repo.delete(req.params.id)
        if (result.affected !== 1) {
            res.status(404).end();
            return;
        }

        res.status(204).end()
    } catch (error) {
        console.log(error)
        res.status(500).json(error);

    }
})