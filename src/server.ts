import express from 'express';
import cors from 'cors';
import { elementController } from './controller/ElementController';
import { categoryController } from './controller/CategoryController';
import { productController } from './controller/ProductController';
import { devisController } from './controller/DevisController';
import { commandeController } from './controller/CommandeController';
import { factureController } from './controller/FactureController';
import { manualProductController } from './controller/ManualProductController';

export const server = express();

server.use(express.json())
server.use(cors());

server.use("/api/element", elementController);
server.use("/api/category", categoryController);
server.use("/api/product", productController);
server.use("/api/devis", devisController);
server.use("/api/commande", commandeController);

//This is done manualy
server.use("/api/facture", factureController);
server.use("/api/manual", manualProductController);